const port = 4000;

const compression = require("compression");
const express = require("express");
const express_basic_auth = require("express-basic-auth");

const app = express();

app.use(
    express_basic_auth({
        users:{admin:'admin'},
        challenge:true
     })
);

app.use(compression());
app.use(express.static("./build"));

app.get("/*",(req, res)=>{res.sendFile("./build/index.html")});

app.listen(port, "0.0.0.0", ()=>{
   console.log(`Listening on port: ${port}`);
});