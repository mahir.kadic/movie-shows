import {createAction} from '../common/redux.helpers';

const SET_USER = "SET_USER";

export const setUser = user => createAction(SET_USER, user);