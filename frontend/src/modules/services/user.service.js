
import axios                             from "axios";

const PRODUCTION_URL    = "";
const DEV_URL           = "http://localhost:3000/users/";

const BASE_URL = DEV_URL;

// Add a request interceptor
axios.interceptors.request.use( function ( config ) {
    return config;
}, function ( error ) {
    // Do something with request error
    return Promise.reject( error );
} );
// Add a response interceptorgit
axios.interceptors.response.use( function ( response ) {
    // Do something with response data
    return response;
}, function ( error ) {
    // Do something with response error
    return Promise.reject( error );
} );
var getData = function ( url, token) {
    return axios( {
        method : 'GET',
        url    : BASE_URL + url,
        headers: {
            "Authorization": "Bearer " + token,
            "Content-Type" : "application/json"
        }
    } )
}
var login = function ( url, data ) {
    return axios( {
        method: 'POST',
        url   : BASE_URL + url,
        data  : data,
        headers: {
            "Content-Type" : "application/json"
        }
    } )
}
var postData = function ( url, data, token ) {
    return axios( {
        method : 'POST',
        url    : BASE_URL + url,
        data   : data,
        headers: {
            "Content-Type" : "application/json",
            "Authorization": "Bearer " + token,
        }
    } )
}
var postWithReferer = function ( url, data, token ) {
    return axios( {
        method : 'POST',
        url: BASE_URL + url,
        data   : data,
        headers: {
            "Content-Type" : "application/json",
            "Authorization": "Bearer " + token,
        }
    } )
}
var putData = function ( url, data, token ) {
    return axios( {
        method : 'PUT',
        url: BASE_URL + url,
        data   : data,
        headers: {
            "Authorization": "Bearer " + token,
            "Content-Type" : "application/json"            
        }
    } )
}
var deleteData = function ( url, data, token ) {
    return axios( {
        method : 'DELETE',
        url: BASE_URL + url,
        data,
        headers: {
            "Authorization": "Bearer " + token,
            "Content-Type" : "application/json"
        }
    } )
}
var services = {
    getData,
    postData,
    putData,
    deleteData,
    login,
    postWithReferer
}
export default services;