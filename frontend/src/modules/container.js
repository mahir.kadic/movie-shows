import React, {Component}   from 'react';
import {connect}            from 'react-redux';
import * as movieActions    from './actions/movies.action';
import * as Helpers         from './common/helpers';
import ListComponent        from './components/main/list.component';
import { Row, Col, Button } from 'react-bootstrap';

class ListContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: "movie",
      search:false,
      query:"",
      modalIsOpen:false,
      currentPage: 1
    };

  }
  
  componentDidMount() {
    this.props.getTopTen(1, this.state.toggle);
  }

  componentWillReceiveProps(nextProps){
    var {type, searchQuery} = nextProps;

    if(searchQuery.length>=3 && (this.state.query !== searchQuery || type!== this.state.toggle)){
      this.props.search(searchQuery, 1, type);
      this.setState({search:true, query:searchQuery, toggle:type});

    }else if(this.state.toggle!==type || (searchQuery.length < 3 && this.state.search)){
      this.props.getTopTen(1, type);
      this.setState({toggle: type, search: false});
    }
  }

  viewMore = () =>{
    const { type } = this.props;
    const {currentPage, query} = this.state;
    const nextPage = currentPage + 1;

    if(query.length>=3){
      this.props.search(query, nextPage, type);
      this.setState({search:true, query:query, toggle:type, currentPage:nextPage});

    }else if((query.length < 3 && !this.state.search)){
      this.props.getTopTen(nextPage, type);
      this.setState({toggle: type, search: false, currentPage:nextPage});
    }
  }

  sortList=(list)=>{
    if(list !== null){
      list.sort((a,b)=>{
        return b.vote_average-a.vote_average;
      });
    }
  }

  render() {
    const {topMovies, searchList} = this.props;
    const active = this.state.search? searchList : topMovies ;
    const list = this.state.search? Helpers.getList(searchList.response) : Helpers.getList(topMovies.response);
    if(list!==null)
      this.props.onSearch(list.slice(0,2), this.state.search);

    this.sortList(list);

    return (
      <div id="list" style={{paddingBottom:16}}>
        <ListComponent movies={list} isLoading={active.isLoading} type={this.state.toggle}/>
        <Row>
          <Col lg={12}  md={12} xs={6}>
            <Button block bsStyle="primary" onClick={()=>this.viewMore()}>View more</Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default connect(
  (state) => ({
    topMovies: state.movies.topTen,
    searchList: state.movies.Search
  }),
  { ...movieActions }
)(ListContainer);