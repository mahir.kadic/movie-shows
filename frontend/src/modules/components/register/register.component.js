import React, { Component } from 'react';
import { connect }          from 'react-redux';
import MuiThemeProvider     from 'material-ui/styles/MuiThemeProvider';
import Center               from 'react-center';
import UserService          from "../../services/user.service"
import {Button}             from "react-bootstrap";

import { 
  TextField,
  Dialog,
} from 'material-ui';
import {
  Step,
  Stepper,
  StepLabel,
} from 'material-ui/Stepper';

import styles from './style'
 
class Register extends Component {
 
  state = {
    open: false,
    checked: false,
    finished: false,
    stepIndex: 0,

    first_name:"",
    last_name:"",
    phone_number:"",

    username:"",
    email:"",
    password:"",
    error:""
  };
 
  handleOpen = () => {
    this.setState({open: true});
  };
 
  handleClose = () => {
    this.setState({open: false, error:""});
  };
 
  handleSubmit = () => {
    let {username, first_name, last_name, password, email, phone_number} = this.state;
    UserService.postData("register",{
      username:username,
      firstName:first_name,
      lastName:last_name,
      password:password,
      email:email,
      phone_number:phone_number
    })
      .then(res=>{
        this.setState({error:"You have successfully registered."})
      })
      .catch(err=>
        this.setState({error:"User already exists or data is invalid"})
      )
  };

  updateCheck() {
    this.setState((oldState) => {
      return {
        checked: !oldState.checked,
      };
    });
  }
 
  handleNext = () => {
    const {stepIndex} = this.state;
    if (stepIndex === 1) {
      this.setState({stepIndex: 0, finished: false});
      window.location = "";
    }
    this.setState({
      stepIndex: stepIndex + 1,
      finished: stepIndex > 2,
    });
  };
 
  handlePrev = () => {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  };
 
  _handleTextFieldChange = (e, field) => {
    this.setState({
      [field]: e.target.value
    });
  }

  getStepContent=(stepIndex)=> {
    switch (stepIndex) {
      case 0:
        return <div>
                <TextField
                  floatingLabelText="First Name"
                  floatingLabelStyle={styles.floatingLabelStyle}
                  floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                  underlineFocusStyle={styles.underlineStyle}
                  fullWidth={true}
                  value={this.state.first_name}
                  onChange={(e)=>this._handleTextFieldChange(e, "first_name")}
                /><br />
                <TextField
                  floatingLabelText="Last Name"
                  floatingLabelStyle={styles.floatingLabelStyle}
                  floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                  underlineFocusStyle={styles.underlineStyle}
                  fullWidth={true}
                  value={this.state.last_name}
                  onChange={(e)=>this._handleTextFieldChange(e, "last_name")}
                /><br />
                <TextField
                  floatingLabelText="Phone Number"
                  floatingLabelStyle={styles.floatingLabelStyle}
                  floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                  underlineFocusStyle={styles.underlineStyle}
                  fullWidth={true}
                  value={this.state.phone_number}
                  onChange={(e)=>this._handleTextFieldChange(e, "phone_number")}
                />
            </div>;
      case 1:
        return <div><TextField
                      floatingLabelText="Username"
                      floatingLabelStyle={styles.floatingLabelStyle}
                      floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                      underlineFocusStyle={styles.underlineStyle}
                      fullWidth={true}
                      value={this.state.username}
                      onChange={(e)=>this._handleTextFieldChange(e, "username")}
                    /><br />  
                    <TextField
                      floatingLabelText="Email"
                      floatingLabelStyle={styles.floatingLabelStyle}
                      floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                      underlineFocusStyle={styles.underlineStyle}
                      fullWidth={true}
                      value={this.state.email}
                      onChange={(e)=>this._handleTextFieldChange(e, "email")}
                    /><br />  
                    <TextField
                      type="password"
                      floatingLabelText="Password"
                      floatingLabelStyle={styles.floatingLabelStyle}
                      floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
                      underlineFocusStyle={styles.underlineStyle}
                      fullWidth={true}
                      value={this.state.password}
                      onChange={(e)=>this._handleTextFieldChange(e, "password")}
                    /><br />
              </div>;
      default:
        return 'Your default steper';
    }
  }
 
  render(){
    const { stepIndex } = this.state;
    const contentStyle = {margin: '0 16px'};
    const actions = [      
      <Button style={styles.buttonStyle} bsStyle="primary" onClick={this.handlePrev} disabled={stepIndex === 0}>Back</Button>
      ,
      <Button 
        style={styles.buttonStyle} 
        bsStyle="primary" 
        onClick={stepIndex === 1 ? this.handleSubmit : this.handleNext} 
      >
          {stepIndex === 1 ? 'Submit' : 'Next'}
      </Button>      
      ,
      <Button style={styles.buttonStyle} bsStyle="primary" onClick={this.handleClose}>Cancel</Button>
    ];
 
    return (
      <MuiThemeProvider>
        <div>
          <Center>
            <Button style={styles.buttonStyle} bsStyle="primary" onClick={this.handleOpen}>Sign Up</Button>
          </Center>        
          <Dialog
            title="Sign Up To Mistral movies "
            actions={actions}
            modal={true}
            open={this.state.open}
            contentStyle={styles.customContentStyle}
          >
          <Stepper activeStep={stepIndex}>
            <Step>
              <StepLabel>
                Basic Info
              </StepLabel>
            </Step>
            <Step>
              <StepLabel>
                Set Login Info
              </StepLabel>
            </Step>
          </Stepper>
          <div style={contentStyle}>
              <div>
                {this.getStepContent(stepIndex)}
              </div>
            </div>
          <p>{this.state.error}</p>
          </Dialog>
        </div>
      </MuiThemeProvider>
    )
  }
}
function mapStateToProps(state){
  return {
    count: state.counterReducer,
  };
}
export default connect(mapStateToProps)(Register);