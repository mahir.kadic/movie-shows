import React                        from 'react';
import {Card, CardTitle, CardMedia} from 'material-ui';
import  Rating                      from 'material-ui-rating'
import { withRouter }               from 'react-router-dom';
import { connect }                  from 'react-redux';
import ReviewService                from "../../services/review.service"

const styles = {
  cardTitle: {
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden'
  },
  cardMedia: {
    maxHeight: 394,
    overflow: 'hidden'
  },
  card: {
    cursor: 'pointer',
    //height: 400,
    overflow: 'hidden'
  },
  bgImage: {
    width: '100%'
  }
};

class CardComponent extends React.Component {
  constructor(props) {
    super(props);
    // Track if the mouse hovering over the movie card 
    this.state = {
      isMouseOver: false,
      vote_average:0
    };
  }
  
  componentDidMount(){
    let {vote_average, id} = this.props.movie;
    this.setState({vote_average:vote_average})
    ReviewService.getData("",this.props.user.token)
      .then( async res=>{
        let el = await res.data.find(element => {
          return element.show_id == id;
        })
        if(typeof el !== "undefined"){
          var sum = el.avg_rating + (vote_average/2);
          this.setState({vote_average:sum})          
        }
      })
      .catch(err => {
        console.log(err);
      })
  }

  _rateMovie=(show, value)=>{
    ReviewService.postData("create",{
      "show_id":show.id,
      "show_name":show.title,
      "type":this.props.type,
      "vote":value
    },this.props.user.token)
      .then(res=>{
        console.log(res);
      })
      .catch(err=>{
        console.log(err);
      })
  }

  render() {
    const {movie, type} = this.props;
    const {vote_average} = this.state;
    const subtitle = this.state.isMouseOver ? movie.overview : null;
    const title = type ==="movie" ? movie.title : movie.name;
    return (
      <div style={{display:"inline"}} id="card" onClick={()=>this.setState({modalIsOpen:true})}>
        <Card
          style={styles.card}
          onMouseOver={() => this.setState({isMouseOver: true})}
          onMouseLeave={() => this.setState({isMouseOver: false})}
        >
          <CardMedia
            style={styles.cardMedia}
            overlay={
              <CardTitle
                title={title} 
                subtitle={subtitle} 
              />
            }
          >
            <img style={styles.bgImage} src={movie.poster_path} />
          </CardMedia>
        </Card>
          <Rating
              value={vote_average/2}
              max={5}
              onChange={(value) => this._rateMovie(movie, value)}
          />
      </div>
    );
  }
}


function mapStateToProps(state){
  return {
    user: state.movies.user
  };
}
export default connect(mapStateToProps)(withRouter(CardComponent));