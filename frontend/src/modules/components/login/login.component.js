import React, { Component } from 'react';
import { connect }          from 'react-redux';
import Center               from 'react-center';
import MuiThemeProvider     from 'material-ui/styles/MuiThemeProvider';
import UserService          from "../../services/user.service"
import { withRouter }       from 'react-router-dom';
import { setUser }          from "../../actions/index";
import { TextField, Dialog} from 'material-ui';
import styles               from './style'
import { createMuiTheme }   from '@material-ui/core/styles';
import blue                 from '@material-ui/core/colors/blue';
import {Button}             from "react-bootstrap";

const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
});

class Login extends Component {
  state = {
    open: false,
    username:"",
    password:"",
    error:""
  };
 
  handleOpen = () => {
    this.setState({open: true});
  };
 
  handleClose = () => {
    this.setState({open: false, error:""});
  };
 
  handleLogin = () => {
    UserService.login("authenticate",{
      username:this.state.username, 
      password:this.state.password
    }).then(res=>{
      this.setState({error:""});
      this.props.setUser(res.data);
      this.props.history.push("/movies");
    }).catch(err=>{
      this.setState({error:"Username or password is incorect"});
    });
  };

  updateCheck() {
    this.setState((oldState) => {
      return {
        checked: !oldState.checked,
      };
    });
  }

  _handleTextFieldChange = (e, field) => {
    this.setState({
      [field]: e.target.value
    });
  }

  render(){
    const actions = [
      <Button style={styles.buttonStyle} bsStyle="primary" onClick={this.handleClose} >Cancel</Button>
      ,
      <Button style={styles.buttonStyle} bsStyle="primary" onClick={this.handleLogin} >Submit</Button>
    ];
 
    return (
      <MuiThemeProvider theme={theme}>
        <div>
        <Center> 
            <Button style={styles.buttonStyle} bsStyle="primary" onClick={this.handleOpen} >Login</Button>
        </Center>  
        <Dialog
          title="Sign In To Mistral movies "
          actions={actions}
          modal={true}
          open={this.state.open}
          contentStyle={styles.customContentStyle}
        >
          <TextField
            floatingLabelText="Username or Email"
            floatingLabelStyle={styles.floatingLabelStyle}
            floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
            underlineFocusStyle={styles.underlineStyle}
            fullWidth={true}
            ref="userName"
            onChange={(e)=>this._handleTextFieldChange(e, "username")}
          /><br />
          <TextField
            type="password"
            floatingLabelText="Password"
            floatingLabelStyle={styles.floatingLabelStyle}
            floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
            underlineFocusStyle={styles.underlineStyle}
            fullWidth={true}
            ref="password"
            onChange={(e)=>this._handleTextFieldChange(e, "password")}
          /><br />
          <p>{this.state.error}</p>
        </Dialog>
        </div>
      </MuiThemeProvider>
    )
  }
}
function mapStateToProps(state){
  return {
    count: state.counterReducer,
  };
}
export default connect(mapStateToProps,{setUser})(withRouter(Login));