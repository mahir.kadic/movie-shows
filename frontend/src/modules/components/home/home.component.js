import React, { Component }   from 'react';
//import { Link }               from 'react-router-dom';
import Login                  from '../login/login.component';
import Register               from '../register/register.component';
 
class Home extends Component {
  render(){   
    return (
      <div>
        <Login />
        <Register />
        {/*<Link to="/movies">Movies</Link>*/}
      </div>
    )
  }
}

export default Home;