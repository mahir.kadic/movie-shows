import {combineReducers}                      from 'redux';
import { createAsyncReducer }  from '../common/redux.helpers';
import { keys as movieActionKeys }            from '../actions/movies.action';

const moviesSuccessReducer = (state, action) => {
  let {results} = action.response;
  const existingArray = state.response && state.request.page!==1 ? state.response.results : [];
  const sliceLength = results.length > 10 ? 10 : results.length;
  return {
    ...state,
    isLoading: false,
    response: {
      ...action.response,
      results: [
        ...existingArray,
        ...results.slice(0,sliceLength)
      ]
    }
  };
}

let initialUser = {
  name : "",
  email: "",
  phone: "",
  token:""
};

const user = ( state = initialUser, action ) => {
  switch ( action.type ) {
      case "SET_USER":
          return { ...action };
      default:
          return state;
  }
};

const moviesReducer = combineReducers({
  topTen: createAsyncReducer(movieActionKeys.GET_TOP_TEN, {
    [`${movieActionKeys.GET_TOP_TEN}_SUCCESS`]: moviesSuccessReducer
  }),
  Search: createAsyncReducer(movieActionKeys.SEARCH, {
    [`${movieActionKeys.SEARCH}_SUCCESS`]: moviesSuccessReducer
  }),
  user:user,
  
});

export default moviesReducer;