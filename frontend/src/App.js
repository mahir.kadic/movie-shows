import React, { Component } from 'react';
import Main                 from "./modules/components/main/main.component"

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.child = React.createRef();
  }

  render() {
    return (
        <Main/>
      );
  }
}

export default App;