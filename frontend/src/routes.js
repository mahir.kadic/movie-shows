import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
 
import App  from './App';
import Home from './modules/components/home/home.component';
 
const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/'           component={Home} />
            <Route exact path='/movies'    component={App} />
        </Switch>
    </BrowserRouter>
);
 
export default Routes;