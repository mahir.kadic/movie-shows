# movie-shows

This project consists of backend, and frontend for movies and TV-shows.
Supports search, rating.

Backend and frontend are divided into 2 seperate folders:

Installation:
    1. git clone https://gitlab.com/mahir.kadic/movie-shows.git
    2. cd movie-shows
    
    Backend: cd backend && npm install && npm start
    
    Frontend: cd frontend && npm install && npm start