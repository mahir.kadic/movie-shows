const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    show_id: { type: String, unique: true, required: true },
    type: { type: String, required: false },
    reviews: { type: Array }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Review', schema);