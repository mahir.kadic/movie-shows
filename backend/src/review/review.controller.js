const express = require('express');
const router = express.Router();
const reviewService = require('./review.service');

// routes
router.get('/', getAll);
router.get('/:id', getById);
router.put('/:id', update);

router.post('/create', create);
router.delete('/:id', _delete);

module.exports = router;

function create(req, res, next) {
    reviewService.create(req.body, req.user)
        .then((review) => res.json(review))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    reviewService.getAll()
        .then(reviews => res.json(reviews))
        .catch(err => next(err));
}

function getById(req, res, next) {
    reviewService.getById(req.params.id)
        .then(review => review ? res.json(review) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    reviewService.update(req.params.id, req.body)
        .then(() => res.json({"success":"Uspjesno"}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    reviewService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}