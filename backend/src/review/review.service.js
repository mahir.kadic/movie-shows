const db            = require('../helpers/db');
const Review        = db.Review;
const User          = db.User;

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function authenticate({ id }) {
    const review = await Review.findOne({ id });
    return review;
}

async function getAll() {
    let arr = await Review.find();
    let resArr= [];
    arr.map(item => {
        let obj = {};
        let sum = 0;
        obj["show_id"] = item.show_id;
        item.reviews.forEach(item=>{
            sum += item.vote;
        });
        obj["avg_rating"] = sum/item.reviews.length;
        resArr.push(obj);
    });
    return resArr;
}

async function getById(id) {
    return await Review.findOne({show_id:id});
}

async function create(reviewParam, user) {
    let review = await getById(reviewParam.show_id);
    let currentUser = await User.findOne({_id:user.sub});
    if(review){
        review.reviews.push({
            vote:reviewParam.vote,
            user:currentUser
        });
        Object.assign(review, reviewParam)
        await review.save();
    }else{
        const review = new Review(reviewParam);
        review.reviews.push({
            vote:reviewParam.vote,
            user:currentUser
        });
        await review.save();
    }

}

async function update(id, reviewParam) {
    const review = await Review.findById(id);

    if (!review) throw 'Review not found';

    await review.save();
}

async function _delete(id) {
    let review = await Review.findOne({show_id:id});
    await Review.findByIdAndRemove(review._id);
}