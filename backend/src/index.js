require('rootpath')();
const express       = require('express');
const app           = express();
const cors          = require('cors');
const bodyParser    = require('body-parser');
const jwt           = require('src/helpers/jwt.js');
const errorHandler  = require('src/helpers/error-handler');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
app.use(jwt());

// api routes
app.use('/users', require('./users/user.controller'));
app.use('/reviews', require('./review/review.controller'));

// global error handler
app.use(errorHandler);

// start index
const port = process.env.NODE_ENV === 'production' ? (process.env.PORT || 80) : 3000;
const index = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});